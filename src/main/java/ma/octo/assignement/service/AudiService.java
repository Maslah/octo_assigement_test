package ma.octo.assignement.service;

import ma.octo.assignement.domain.util.EventType;

/**
 * 
 * @Amine
 *
 */
public interface AudiService {
	
	public void auditVirementOrVersement(String message,EventType eventType );

}
