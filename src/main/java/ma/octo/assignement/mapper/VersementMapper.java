package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {

    private static Versement versement;
    private static VersementDto versementDto;


    public static VersementDto map(Versement versement) {
        versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur(versement.getNomPrenomEmetteur());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMotif(versement.getMotifVersement());
        versementDto.setMontantVersement(versement.getMontantVersement());
        versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());
        return versementDto;
    }

    public static Versement VersementDtoToVersement(VersementDto versementDto) {
        versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setMotifVersement(versementDto.getMotif());
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
        return versement;
    }
}
