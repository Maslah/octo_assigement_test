package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.Audit;

/**
 * 
 * @amine
 *
 */
public interface AuditRepository extends JpaRepository<Audit, Long> {
}
