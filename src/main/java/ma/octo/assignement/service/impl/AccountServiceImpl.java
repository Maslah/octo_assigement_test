package ma.octo.assignement.service.impl;


import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.AppRoleRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {


    @Autowired
    private AppRoleRepository appRoleRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Utilisateur addNewUser(Utilisateur user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return utilisateurRepository.save(user);

    }

    @Override
    public AppRole addNewRole(AppRole appRole) {

        return  appRoleRepository.save(appRole);

    }

    @Override
    public void addRoleToUser(String username, String roleName) {

        Utilisateur user = utilisateurRepository.findByUsername(username);
        AppRole role = appRoleRepository.findByRoleName(roleName);
        if(user!=null && role!=null)
        {
            user.getRoles().add(role);
            utilisateurRepository.save(user);
        }


    }

    @Override
    public Utilisateur loadUserByUsername(String username) {

        Utilisateur user = utilisateurRepository.findByUsername(username);

        return user;
    }

    @Override
    public List<Utilisateur> listUsers() {

        return utilisateurRepository.findAll();
    }
}
