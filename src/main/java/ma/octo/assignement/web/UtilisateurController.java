package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/users")
public class UtilisateurController {

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateurs() {
       return utilisateurService.loadAllUtilisateur();
    }
}
