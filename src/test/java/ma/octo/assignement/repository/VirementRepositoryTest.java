package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;



  @Test
  public void findOne() {

    Virement virement = virementRepository.findById(6L).get();

    assertThat(virement.getId()).isEqualTo(6L);
  }

  @Test
  public void findAll() {

   List<Virement> virementList = virementRepository.findAll();

    assertThat(virementList.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
    Compte c1 = new Compte(1L,"010000A000001000","rib1",BigDecimal.valueOf(100000));
    Compte f12 = new Compte(2L,"020000A000001000","rib2",BigDecimal.valueOf(200000));
    Virement virement = new Virement(null,BigDecimal.valueOf(1000),new Date(),c1,f12,"virement 2021");
    virementRepository.save(virement);

    assertThat(virement.getId()).isGreaterThan(0);
  }

  @Test
  public void delete() {
    Virement virement = virementRepository.findById(6L).get();

    virementRepository.delete(virement);
    Virement v =null;
    Optional<Virement> optionalVirement = Optional.of(virementRepository.findById(6L).get());

    if(optionalVirement.isPresent()){
      v = optionalVirement.get();
    }
    assertThat(v).isNotNull();

  }
}