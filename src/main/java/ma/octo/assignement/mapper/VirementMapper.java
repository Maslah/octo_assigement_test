package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {


    private static VirementDto virementDto;
    private static Virement virement;


    public static VirementDto map(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());

        return virementDto;
    }

    public static Virement VirementDtoToVirement(VirementDto virementDto) {
        virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());

        return virement;
    }
}
