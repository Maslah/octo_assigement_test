package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utilisateur user=accountService.loadUserByUsername(username);

        Collection<GrantedAuthority> authorities = user.getRoles().stream().map(r ->new SimpleGrantedAuthority(r.getRoleName())).collect(Collectors.toList());

        return new User(user.getUsername(),user.getPassword(), authorities);
    }
}
