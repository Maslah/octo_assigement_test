package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AudiService;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class VirementServiceImpl implements VirementService {



    public static final int MONTANT_MINIIMAL = 10;

    public static final int MONTANT_MAXIMAL = 10000000;

    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private AudiService monservice;

    @Override
    public List<VirementDto> loadAllVirements() {
        List<Virement> virementList = virementRepository.findAll();
        List<VirementDto> virementDtoList = virementList.stream().map(virement ->
                VirementMapper.map(virement)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(virementDtoList)) {
            return null;
        } else {
            return virementDtoList;
        }
    }

    @Override
    public VirementDto createTransaction(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte compteEmetteur= compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte introuvable");
        }

        if (compteBeneficiaire == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte introuvable");
        }

        if (virementDto.getMontantVirement() == null) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() == 0) {
           LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < MONTANT_MINIIMAL) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif() == null) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Pas de solde pas de virement");
        }

        /*if (c1.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }*/

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = VirementMapper.VirementDtoToVirement(virementDto);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBeneficiaire);

        virementRepository.save(virement);


        monservice.auditVirementOrVersement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString(), EventType.VIREMENT);

           return virementDto;


    }
    private void save(Virement Virement) {
        virementRepository.save(Virement);
    }
}
