package ma.octo.assignement.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Amine
 * 
 * Fixed javax.persistence.*;
 * 
 */

@Entity
@Table(name = "UTILISATEUR")
public class Utilisateur implements Serializable {
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

@Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 10, nullable = false, unique = true)
  private String username;

  @Column(length = 10, nullable = false)
  private String gender;

  @Column(length = 60, nullable = false)
  private String lastname;

  @Column(length = 60, nullable = false)
  private String firstname;

  @Temporal(TemporalType.DATE)
  private Date birthdate;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private String password;
  @ManyToMany(fetch = FetchType.EAGER)
  private Collection<AppRole> roles=new ArrayList<>();


  public Utilisateur(){

  }

  public Utilisateur(Long id, String username, String gender, String lastname, String firstname, String password, Collection<AppRole> roles) {
    this.id = id;
    this.username = username;
    this.gender = gender;
    this.lastname = lastname;
    this.firstname = firstname;
    this.password = password;
    this.roles = roles;
  }
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Collection<AppRole> getRoles() {
    return roles;
  }

  public void setRoles(Collection<AppRole> roles) {
    this.roles = roles;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public Date getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
