package ma.octo.assignement.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import ma.octo.assignement.domain.util.EventType;

@Entity
@Table(name = "AUDIT")
public class Audit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private Long id;

	  @Column(length = 100)
	  private String message;

	  @Enumerated(EnumType.STRING)
	  private EventType eventType;

	  public Long getId() {
	    return id;
	  }

	  public void setId(Long id) {
	    this.id = id;
	  }

	  public String getMessage() {
	    return message;
	  }

	  public void setMessage(String message) {
	    this.message = message;
	  }

	  public EventType getEventType() {
	    return eventType;
	  }

	  public void setEventType(EventType eventType) {
	    this.eventType = eventType;
	  }

}
