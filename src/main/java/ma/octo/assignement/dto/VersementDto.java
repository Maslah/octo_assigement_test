package ma.octo.assignement.dto;


import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
    private String nomPrenomEmetteur;
    private String nrCompteBeneficiaire;

   // @NotNull(message = "Motif vide")
    private String motif;
    private BigDecimal montantVersement;
    private Date date;


    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public String getNrCompteBeneficiaire() {
        return nrCompteBeneficiaire;
    }

    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public void setMontantVersement(BigDecimal montantVersement) {
        this.montantVersement = montantVersement;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
