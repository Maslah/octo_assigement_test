package ma.octo.assignement.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AudiService;

/**
 * @Amine
 *
 */

@Service
@Transactional
public class AudiServiceImpl implements AudiService {

    Logger LOGGER = LoggerFactory.getLogger(AudiServiceImpl.class);

    @Autowired
    private AuditRepository auditRepository;

    @Override
    public void auditVirementOrVersement(String message, EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        Audit audit = new Audit();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditRepository.save(audit);
    }


}
