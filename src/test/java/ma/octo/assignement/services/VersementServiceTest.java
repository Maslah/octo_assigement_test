package ma.octo.assignement.services;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AudiService;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.service.impl.VersementServiceImpl;
import ma.octo.assignement.service.impl.VirementServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@Transactional
public class VersementServiceTest {

    @Mock
    private VersementRepository versementRepository;
    @Mock
    private CompteRepository compteRepository;

    @InjectMocks
    private VersementServiceImpl versementService;

    @Mock
    private AudiService audiService;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        //versementService = new VersementServiceImpl(versementRepository,compteRepository);
    }


    @Test
    public void canGetAllVersements() {
        List<Versement> versementList = new ArrayList<>();
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");
        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        Versement v = new Versement();
        v.setMontantVersement(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setDateExecution(new Date());
        v.setMotifVersement("Assignment 2021 versement");
        //when
        versementList.add(v);
        when(versementRepository.findAll()).thenReturn(versementList);

        List<VersementDto> result =   versementService.loadAll();

        Assertions.assertEquals(1, result.size());
        //then
        verify(versementRepository).findAll();
    }
    @Test
    public void VersementTransactionShouldCreated() throws TransactionException, SoldeDisponibleInsuffisantException, CompteNonExistantException {

        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(BigDecimal.valueOf(1000));
        versementDto.setDate(new Date());
        versementDto.setMotif("Versement test");
        versementDto.setNrCompteBeneficiaire("010000A000001000");
        versementDto.setNomPrenomEmetteur("Amine");

        Compte beneficiareCompte = new Compte(1L,"010000A000001000","rib1",BigDecimal.valueOf(100000));

       when(compteRepository.findByNrCompte(beneficiareCompte.getNrCompte())).thenReturn(beneficiareCompte);


       versementService.createTransaction(versementDto);

       Assertions.assertEquals("010000A000001000",beneficiareCompte.getNrCompte());


    }
}
