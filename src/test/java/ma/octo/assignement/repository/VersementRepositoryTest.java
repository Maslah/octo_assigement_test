package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {

  @Autowired
  private VersementRepository versementRepository;

  
  @Test
  public void findOne() {

    Versement versement = versementRepository.findById(7L).get();

    assertThat(versement.getId()).isEqualTo(7L);
  }

  @Test
  public void findAll() {
    List<Versement> versementList = versementRepository.findAll();

    assertThat(versementList.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
    Compte c1 = new Compte(1L,"010000A000001000","rib1", BigDecimal.valueOf(100000));
    Versement versement = new Versement(null,BigDecimal.valueOf(1000),new Date(),"amine",c1,"motif versemet");
    versementRepository.save(versement);

    assertThat(versement.getId()).isGreaterThan(0);
  }

  @Test
  public void delete() {
    Versement versement = versementRepository.findById(7L).get();

    versementRepository.delete(versement);
    Versement v =null;
    Optional<Versement> optionalVersement = Optional.of(versementRepository.findById(7L).get());

    if(optionalVersement.isPresent()){
      v = optionalVersement.get();
    }
    assertThat(v).isNotNull();

  }
}