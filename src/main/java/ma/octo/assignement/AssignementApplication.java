package ma.octo.assignement;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VirementRepository virementRepository;

	@Autowired
	private VersementRepository versementRepository;

	@Autowired
	private AccountService accountService;
	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}


	@Bean
	PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}
	@Override
	public void run(String... strings) throws Exception {


		accountService.addNewRole(new AppRole(null,"USER"));
		accountService.addNewRole(new AppRole(null,"ADMIN"));


		Utilisateur admin = accountService.addNewUser(new Utilisateur(null,"admin","Male","admin","admin","1234",new ArrayList<>()));
		Utilisateur user1 = accountService.addNewUser(new Utilisateur(null,"user1","Male","last1","first1","1234",new ArrayList<>()));
		Utilisateur user2 = accountService.addNewUser(new Utilisateur(null,"user2","Male","last2","first2","1234",new ArrayList<>()));


		// role affectation
		accountService.addRoleToUser("user1","USER");
		accountService.addRoleToUser("user2","USER");
		accountService.addRoleToUser("admin","USER");
		accountService.addRoleToUser("admin","ADMIN");




		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(user1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(user2);

		compteRepository.save(compte2);

		Virement v = new Virement();
		v.setMontantVirement(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifVirement("Assignment 2021");


		virementRepository.save(v);

		//versement
		Versement versement = new Versement(null,BigDecimal.valueOf(1000),new Date(),"amine",compte1,"motif versemet");

		versementRepository.save(versement);
	}
}
