package ma.octo.assignement.services;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AudiService;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.service.impl.AudiServiceImpl;
import ma.octo.assignement.service.impl.VirementServiceImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@Transactional
public class VirementServiceTest {

    @Mock
    private VirementRepository virementRepository;
    @Mock
    private CompteRepository compteRepository;

    @InjectMocks
    private VirementServiceImpl virementService;

    @Mock
    private AudiService audiService;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        //virementService = new VirementServiceImpl(virementRepository,compteRepository,audiService );
    }


    @Test
    public void canGetAllVirements() {
        List<Virement> virementList = new ArrayList<>();
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");
        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        Virement v = new Virement();
        v.setMontantVirement(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateExecution(new Date());
        v.setMotifVirement("Assignment 2021");
        Virement v1 = new Virement();
        v1.setMontantVirement(BigDecimal.TEN);
        v1.setCompteBeneficiaire(compte1);
        v1.setCompteEmetteur(compte2);
        v1.setDateExecution(new Date());
        v1.setMotifVirement("Assignment 2022");
   //when
        virementList.add(v);
        virementList.add(v1);
        when(virementRepository.findAll()).thenReturn(virementList);

        List<VirementDto> result =   virementService.loadAllVirements();

        Assertions.assertEquals(2, result.size());
        //then
        verify(virementRepository).findAll();
    }

    @Test
    public void VirementTransactionShouldCreated() throws TransactionException, SoldeDisponibleInsuffisantException, CompteNonExistantException {

        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(BigDecimal.valueOf(1000));
        virementDto.setDate(new Date());
        virementDto.setMotif("virement test");
        virementDto.setNrCompteBeneficiaire("010000A000001000");
        virementDto.setNrCompteEmetteur("020000A000001000");

        Compte compteEmetteur = new Compte(1L,"010000A000001000","rib1",BigDecimal.valueOf(100000));
        Compte compteBenifiaciere = new Compte(2L,"020000A000001000","rib2",BigDecimal.valueOf(200000));

        when(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).thenReturn(compteEmetteur);
        when(compteRepository.findByNrCompte(compteBenifiaciere.getNrCompte())).thenReturn(compteBenifiaciere);


        VirementDto result = virementService.createTransaction(virementDto);

        Assertions.assertNotNull(result);

        Assertions.assertEquals("010000A000001000", result.getNrCompteBeneficiaire());


    }

}
