package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface VersementService {
    List<VersementDto> loadAll();
    void createTransaction(@RequestBody VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;

}
