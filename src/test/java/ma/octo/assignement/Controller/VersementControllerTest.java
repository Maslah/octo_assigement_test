package ma.octo.assignement.Controller;

import ma.octo.assignement.service.VersementService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import ma.octo.assignement.web.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = VersementController.class)
public class VersementControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllVersements() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/listeversements"))
                .andExpect(status().isOk());
    }
}

