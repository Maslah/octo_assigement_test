package ma.octo.assignement.service;


import ma.octo.assignement.domain.AppRole;
import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface AccountService {

    Utilisateur addNewUser(Utilisateur user);
    AppRole addNewRole(AppRole appRole);
    void addRoleToUser(String username,String roleName);
    Utilisateur loadUserByUsername(String username);
    List<Utilisateur> listUsers();


}
