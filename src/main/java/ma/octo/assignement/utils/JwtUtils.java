package ma.octo.assignement.utils;

public class JwtUtils {
    public static final String SECRET = "my-secret-key";
    public static final String AUTH_HEAD = "Authorization";
    public static final String PREFIXE = "Bearer ";

    public static final long EXPIRE_ACCESS_TOKEN =30*60*1000 ; // 30 min
    public static final long EXPIRE_REFRESH_TOKEN =120*60*1000 ; // 2h
}
