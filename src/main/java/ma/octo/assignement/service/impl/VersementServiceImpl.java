package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AudiService;
import ma.octo.assignement.service.VersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VersementServiceImpl implements VersementService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIIMAL = 10;

    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImpl.class);

    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private AudiService monservice;

    public VersementServiceImpl(VersementRepository versementRepository, CompteRepository compteRepository) {
        this.versementRepository = versementRepository;
        this.compteRepository = compteRepository;
    }

    @Override
    public List<VersementDto> loadAll() {
        List<Versement> versements = versementRepository.findAll();
        List<VersementDto> versementDtos = versements.stream().map(versement -> VersementMapper.map(versement)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(versementDtos)) {
            return null;
        } else {
            return versementDtos;
        }
    }

    @Override
    public void createTransaction(VersementDto versementDto)  throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{


        Compte beneficiareCompte = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());


        if (beneficiareCompte == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte introuvable");
        }

        if (versementDto.getMontantVersement() == null) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < MONTANT_MINIIMAL) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (versementDto.getMotif().length()  ==  0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        beneficiareCompte.setSolde(new BigDecimal(beneficiareCompte.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(beneficiareCompte);

        Versement versement = VersementMapper.VersementDtoToVersement(versementDto);
        versement.setCompteBeneficiaire(beneficiareCompte);


        versementRepository.save(versement);

        monservice.auditVirementOrVersement("Versement depuis " + versement.getNomPrenomEmetteur() + " vers " + versementDto
            .getNrCompteBeneficiaire() + " d'un montant de " + versement.getMotifVersement().toString(), EventType.VERSEMENT);


    }
}
